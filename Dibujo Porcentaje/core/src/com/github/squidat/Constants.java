package com.github.squidat;

import com.badlogic.gdx.graphics.Color;

/**
 * Created by juan on 25/08/17.
 */

public class Constants {
    public static final int GRID_DIMENSION = 5;
    public static final Color GRID_COLOR = Color.WHITE;
    public static final Color CASILLA_COLOR = Color.LIGHT_GRAY;

}
