package com.github.squidat;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.reflect.Constructor;

public class DibujoPorcentaje extends ApplicationAdapter {
	public final String TAG = getClass().getSimpleName();
	public ShapeRenderer renderer;
	TableroRandom tablero;

	@Override
	public void create () {
		renderer = new ShapeRenderer();
		Vector2 pos = Tablero.calcularPosicionTablero();
		float lado_tablero = Math.min(Gdx.graphics.getHeight(),Gdx.graphics.getWidth());
		tablero = new TableroRandom(pos.x,pos.y,lado_tablero,5);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		if (Gdx.input.justTouched()) {
			tablero.randomizeColors();
		}
		tablero.draw(renderer);
	}

	@Override
	public void dispose () {
		renderer.dispose();
	}
}
