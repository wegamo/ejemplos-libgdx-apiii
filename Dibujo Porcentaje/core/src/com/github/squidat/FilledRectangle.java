package com.github.squidat;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by juan on 13/09/17.
 */

public class FilledRectangle extends Rectangle {
    private Color borde, relleno;

    public FilledRectangle(float x, float y, float lado_casilla, Color borde , Color relleno) {
        super(x,y,lado_casilla,lado_casilla);
        this.borde = borde;
        this.relleno = relleno;
    }

    public void draw(ShapeRenderer renderer) {
        //Dibujamos el fondo primero para que el borde se pinte encima
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.setColor(relleno);
        renderer.box(getX(),getY(),0,getWidth(),getHeight(),0);
        renderer.end();

        renderer.begin(ShapeRenderer.ShapeType.Line);
        renderer.setColor(borde);
        renderer.box(getX(),getY(),0,getWidth(),getHeight(),0);
        renderer.end();
    }

    public Color getBorde() {
        return borde;
    }

    public void setBorde(Color borde) {
        this.borde = borde;
    }

    public Color getRelleno() {
        return relleno;
    }

    public void setRelleno(Color relleno) {
        this.relleno = relleno;
    }
}
