package com.github.squidat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by juan on 13/09/17.
 */

public class Tablero {
    private Array<FilledRectangle> casillas;
    private int dimension;
    private float lado_casilla;
    private float x;
    private float y;

    public Tablero(float x, float y,float ancho_tablero, int dimension) {
        this.x = x;
        this.y = y;
        this.dimension = dimension;
        this.lado_casilla = ancho_tablero/dimension;
        this.crearCasillas();
    }

    protected void crearCasillas() {
        /*
         * Crea y llena la lista de casillas que pertenecen al tablero.
         * Por ahora asume que siempre seran cuadrados.
         */

        casillas = new Array<FilledRectangle>();
        float xMove = this.x;
        float yMove = this.y;

        for (int i=0; i < dimension; i++) {
            for (int j=0; j <dimension; j++) {
                FilledRectangle rectangle = new FilledRectangle(xMove,yMove,lado_casilla, Color.WHITE,Color.DARK_GRAY);
                casillas.add(rectangle);
                xMove += lado_casilla;
            }
            yMove += lado_casilla;
            xMove = this.x;
        }
    }

    public static Vector2 calcularPosicionTablero() {
        /* Calcula las coordenadas XY para que el tablero quede centrado en la pantalla */

        float screen_width = Gdx.graphics.getWidth();
        float screen_height = Gdx.graphics.getHeight();

        float x = 0, y = 0;
        if (screen_height > screen_width) {
            y = (screen_height - screen_width)/2;
        } else if (screen_width > screen_height) {
            x = (screen_width - screen_height)/2;
        }

        return new Vector2(x,y);
    }

    public void draw(ShapeRenderer renderer) {
        for (FilledRectangle casilla: casillas) {
            casilla.draw(renderer);
        }
    }

    public Array<FilledRectangle> getCasillas() {
        return casillas;
    }

    public void setCasillas(Array<FilledRectangle> casillas) {
        this.casillas = casillas;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public float getLadoCasilla() {
        return lado_casilla;
    }

    public void setLadoCasilla(float lado_casilla) {
        this.lado_casilla = lado_casilla;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
