package com.github.squidat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

/**
 * Created by juan on 13/09/17.
 */

public class TableroRandom extends Tablero {
    Random random;

    public TableroRandom(float x, float y, float ancho_tablero, int dimension) {
        super(x, y, ancho_tablero, dimension);
        random = new Random();
        this.randomizeColors();
    }

    public void randomizeColors() {
        for (FilledRectangle rectangle: getCasillas()) {
            Color relleno = new Color(random.nextFloat(),random.nextFloat(),random.nextFloat(),1);
            rectangle.setRelleno(relleno);
        }
    }
}
