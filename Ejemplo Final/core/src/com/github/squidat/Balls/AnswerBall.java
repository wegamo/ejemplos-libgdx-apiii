package com.github.squidat.Balls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.github.squidat.Constants;

import java.util.Random;

/**
 * Created by juan on 30/08/17.
 */

public class AnswerBall extends MessageBall {
    private final String TAG = getClass().getSimpleName();
    private Random random;
    private int value;
    private boolean moving;
    private Sound correct, incorrect;


    public AnswerBall(float radius, int value, Color balll_color, Texture graphic, BitmapFont font,Color font_color, Sound correct, Sound incorrect) {
        super(Constants.WORLD_WIDTH/2 - radius,Constants.WORLD_HEIGHT, radius, String.valueOf(value), balll_color,graphic, font, font_color);
        this.correct = correct;
        this.incorrect = incorrect;
        this.value = value;
        random = new Random();

        this.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                moving = true;
                return true;
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if (moving) {
                    Vector2 basePosition = new Vector2(Gdx.input.getX(), Gdx.input.getY());
                    Vector2 traduced = getStage().screenToStageCoordinates(basePosition);

                    float finalX = MathUtils.clamp(traduced.x - getRadius(), getRadius(), Constants.WORLD_WIDTH - 2 * getRadius());
                    float finalY = MathUtils.clamp(traduced.y - getRadius(), getRadius(), Constants.WORLD_HEIGHT - 2 * getRadius());

                    MoveToAction smoothMove = new MoveToAction();
                    smoothMove.setPosition(finalX,finalY);
                    smoothMove.setDuration(Constants.FOLLOW_DELAY);
                    addAction(smoothMove);
                }
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                moving = false;
            }
        });

        this.addAction(new MoveToCenterAction());
    }


    public boolean answer(NumberQuestionBall question) {
        if (question.checkAnswer(this.value)) {
            Gdx.app.log(TAG,"Correcto!");
            correctAnswer(question);
            return true;
        } else {
            Gdx.app.log(TAG, "Incorrecto!");
            wrongAnswer();
            return false;
        }
    }

    private void correctAnswer(NumberQuestionBall question) {
        /* Recibe un NumberQuestionBall para saber a donde se debe mover el AnswerBall en la animacion */

        moving = false;
        correct.play();
        Circle questionCircle = question.getBoundingCircle();
        Vector2 circlePosititionBeforeFade = new Vector2(questionCircle.x - this.getRadius(),questionCircle.y - this.getRadius());

        Label label = this.getLabel();
        Vector2 labelPositionBeforeFade = new Vector2(questionCircle.x - label.getWidth()/2,questionCircle.y - label.getHeight()/2);

        this.addAction(new CorrectAnswerAction(circlePosititionBeforeFade),new CorrectAnswerAction(labelPositionBeforeFade));
    }

    private  void wrongAnswer() {
        incorrect.play();
        this.addAction(new MoveToCenterAction());
    }


    protected class CorrectAnswerAction extends SequenceAction {
        public CorrectAnswerAction(Vector2 positionBeforeFade) {
            super();
            MoveToAction moveToFinalPosition = new MoveToAction();
            moveToFinalPosition.setPosition(positionBeforeFade.x,positionBeforeFade.y);
            moveToFinalPosition.setDuration(Constants.CENTER_DURATION);
            this.addAction(moveToFinalPosition);
            this.addAction(Actions.fadeOut(Constants.FADE_DURATION));
            this.addAction(new NewAnswerBallAction());
            this.addAction(new MoveToCenterAction());
        }
    }

    protected class NewAnswerBallAction extends SequenceAction {
        public NewAnswerBallAction() {
            super();
            MoveToAction moveToInitialPosition = new MoveToAction();
            moveToInitialPosition.setPosition(Constants.WORLD_WIDTH/2 - getRadius(),Constants.WORLD_HEIGHT);
            moveToInitialPosition.setDuration(0);
            this.addAction(moveToInitialPosition);
            //Colocamos el uso de el metodo setRandomValue dentro de una nueva Action para que siga el orden
            //del SequenceAction. Si no se coloca, se cambiaria instantaneamente el valor de la AnswerBall
            //cuando todavia es visible.
            this.addAction(new Action() {
                @Override
                public boolean act(float delta) {
                    setRandomValue();
                    return true;
                }
            });
            this.addAction(Actions.fadeIn(0));
        }
    }

    protected class MoveToCenterAction extends SequenceAction {
        /* Tiene la misma funcionalidad que un MoveToAction pero deshabilita el toque a la AnswerBall mientras se mueve */

        public MoveToCenterAction() {
            //Disable touch
            addAction(new Action() {
                @Override
                public boolean act(float delta) {
                    setTouchable(Touchable.disabled);
                    return true;
                }
            });

            MoveToAction moveToCenter = new MoveToAction();
            moveToCenter.setPosition(Constants.WORLD_WIDTH/2 - getRadius(),Constants.WORLD_HEIGHT/2 - getRadius());
            moveToCenter.setDuration(Constants.DROP_DURATION);
            addAction(moveToCenter);

            //Enable touch
            addAction(new Action() {
                @Override
                public boolean act(float delta) {
                    setTouchable(Touchable.enabled);
                    return true;
                }
            });

        }
    }

    private void setRandomValue() {
        setValue(random.nextInt(98) + 1);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
        this.setMessage(String.valueOf(value));
    }
}
