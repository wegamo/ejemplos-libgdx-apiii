package com.github.squidat.Balls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.github.squidat.Constants;
import com.github.squidat.ICircle;
import com.github.squidat.ParImpar;

/**
 * Created by juan on 30/08/17.
 */
public class Ball extends Actor implements ICircle {
    private final String TAG = getClass().getSimpleName();
    private Texture graphic;
    private float radius;
    private Circle boundingCircle;

    public Ball(float x, float y, final float radius, Color color, Texture graphic) {
        this.setColor(color);
        //graphic = ParImpar.assetManager.get("hole.png", Texture.class);
        this.graphic = graphic;
        this.radius = radius;
        this.setBounds(x,y,radius*2,radius*2);
        this.boundingCircle = new Circle(x + radius, y + radius,radius);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(getColor());
        batch.draw(graphic,getX(),getY(),getWidth(),getHeight());
    }

    @Override
    public void act(float delta) {
        //Actualizamos la posicion del BoundingCircle pues no sabemos como el Table maneja la posicion del actor
        //no usa el setPosition() por lo tanto simplemente vamos updateando la posicion del boundingCircle "manualmente".
        boundingCircle.setPosition(getX() + radius, getY() + radius);
        super.act(delta);
    }

    @Override
    public Circle getBoundingCircle() {
        return boundingCircle;
    }


    public boolean collided(ICircle element) {
        return this.boundingCircle.overlaps(element.getBoundingCircle());
    }

    public ICircle collided(Array<ICircle> circles) {
        for (ICircle circle : circles) {
            if (this.collided(circle))
                return circle;
        }
        return null;
    }


    public float getRadius() {
        return radius;
    }
}
