package com.github.squidat.Balls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.github.squidat.Constants;
import com.github.squidat.ParImpar;

/**
 * Created by juan on 30/08/17.
 *
 */
public class MessageBall extends Ball {
    private final String TAG = getClass().getSimpleName();
    private String message;
    private Label.LabelStyle labelStyle;
    private Label label;

    public MessageBall(float x, float y, float radius, String msg, Color ball_color, Texture graphic, BitmapFont font, Color font_color) {
        super(x, y, radius, ball_color, graphic);
        message = msg;
        labelStyle = new Label.LabelStyle(font,font_color);
        label = new Label(message,labelStyle);
        Vector2 fontPos = calculateFontPosition();
        label.setPosition(fontPos.x,fontPos.y);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        Vector2 fontPos = calculateFontPosition();
        label.setPosition(fontPos.x,fontPos.y);
        label.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        label.draw(batch,parentAlpha);
    }

    public Vector2 calculateFontPosition() {
        /* Calcula las coordenadas XY necesarias para dibujar al Label centrado respecto al centro del Ball */

        float fontX = getBoundingCircle().x - label.getWidth()/2;
        float fontY = getBoundingCircle().y - label.getHeight()/2;

        return new Vector2(fontX,fontY);
    }

    public void addAction(Action ball_action, Action label_action) {
        /* El Label tambien es un actor, pero si agregas un Action solo se agregara al actor principal que
        *  es el MessageBall. Con este metodo puedes agregar un Action al MessageBall y a su Label a la vez.
        *
        *  NOTA: Los parametros deben ser objetos diferentes pues si tratas de agregar la misma
        *        accion a diferentes actores, todos aceleraran el delta de la accion.
        **/

        this.addAction(ball_action);
        label.addAction(label_action);
    }

    @Override
    public Circle getBoundingCircle() {
        return super.getBoundingCircle();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        label = new Label(message,labelStyle);
        Vector2 fontPos = calculateFontPosition();
        Gdx.app.log(TAG,String.format("SetMessage - New position: %f,%f",fontPos.x,fontPos.y));
        label.setPosition(fontPos.x,fontPos.y);
    }

    public Label.LabelStyle getLabelStyle() {
        return labelStyle;
    }

    public void setLabelStyle(Label.LabelStyle labelStyle) {
        this.labelStyle = labelStyle;
        label = new Label(message,labelStyle);
    }

    public Label getLabel() {
        return label;
    }
}
