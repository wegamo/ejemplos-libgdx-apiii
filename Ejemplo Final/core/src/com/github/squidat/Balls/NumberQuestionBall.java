package com.github.squidat.Balls;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.github.squidat.Balls.MessageBall;
import com.github.squidat.Questions.NumberQuestion;

/**
 * Created by juan on 03/09/17.
 */

public abstract class NumberQuestionBall extends MessageBall {
    NumberQuestion question;

    public NumberQuestionBall(float x, float y, float radius, String msg, Color color, Texture graphic, BitmapFont font, Color font_color) {
        super(x, y, radius, msg, color, graphic, font, font_color);
    }

    public abstract boolean checkAnswer(int answer);

    public NumberQuestion getQuestion() {
        return question;
    }

    public void setQuestion(NumberQuestion question) {
        this.question = question;
    }

}
