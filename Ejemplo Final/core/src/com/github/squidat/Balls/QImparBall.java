package com.github.squidat.Balls;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.github.squidat.Questions.QImpar;

/**
 * Created by juan on 30/08/17.
 */

public class QImparBall extends NumberQuestionBall {

    public QImparBall(float x, float y, float radius, String msg, Color color, Texture graphic, BitmapFont font, Color font_color) {
        super(x, y, radius, msg, color, graphic, font, font_color);
        this.setQuestion(new QImpar());
        this.setTouchable(Touchable.disabled);
    }

    public boolean checkAnswer(int answer) {
        return getQuestion().checkAnswer(answer);
    }
}
