package com.github.squidat;

/**
 * Created by juan on 30/08/17.
 */

public class Constants {
    public static float WORLD_HEIGHT = 1920f;
    public static float WORLD_WIDTH = 1080f;

    public static String BG_FILE_NAME = "bg1.jpg";
    public static String SCORE_TAG = "Score:     ";

    public static float DROP_DURATION = 1.3f;
    public static float FOLLOW_DELAY = 0.3f;
    public static float FADE_DURATION = 0.4f;
    public static float CENTER_DURATION = 0.5f;

}
