package com.github.squidat;

import com.badlogic.gdx.math.Circle;

/**
 * Created by juan on 30/08/17.
 */

public interface ICircle {
    Circle getBoundingCircle();
}
