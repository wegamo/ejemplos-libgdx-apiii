package com.github.squidat;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.github.squidat.Screens.LoadingScreen;

public class ParImpar extends Game {
	private AssetManager manager;

	@Override
	public void create () {
		manager = new AssetManager();
		this.setScreen(new LoadingScreen(this));
	}

	@Override
	public void dispose () {
		manager.clear();
		manager.dispose();
	}

	public AssetManager getAssetManager() {
		return manager;
	}
}
