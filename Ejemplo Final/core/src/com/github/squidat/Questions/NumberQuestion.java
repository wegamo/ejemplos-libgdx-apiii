package com.github.squidat.Questions;

/**
 * Created by juan on 30/08/17.
 */

public interface NumberQuestion{
    boolean checkAnswer(int answer);
}
