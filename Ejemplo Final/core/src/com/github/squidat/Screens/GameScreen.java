package com.github.squidat.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.squidat.Balls.AnswerBall;
import com.github.squidat.Balls.QImparBall;
import com.github.squidat.Balls.QParBall;
import com.github.squidat.Constants;
import com.github.squidat.ICircle;
import com.github.squidat.ParImpar;
import com.github.squidat.Balls.NumberQuestionBall;
import com.github.squidat.TableroPuntuacion;

import java.util.Random;

/**
 * Created by juan on 06/09/17.
 */

public class GameScreen extends ScreenAdapter {
    private ParImpar game;
    private AssetManager manager;
    private Random random;
    private Stage stage;
    private AnswerBall answer;
    private Array<ICircle> QUESTIONS;
    private TableroPuntuacion tableroPuntuacion;

    public GameScreen(ParImpar game) {
        this.game = game;
        this.manager = game.getAssetManager();
        random = new Random();
        Viewport viewport = new FitViewport(Constants.WORLD_WIDTH,Constants.WORLD_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport);
        //Obtenemos los Assets que se usaran en este Screen
        Texture circleTexture = manager.get("hole.png",Texture.class);
        BitmapFont chunkBig = manager.get("chunkBig.ttf",BitmapFont.class);
        BitmapFont bebasBig = manager.get("bebasBig.ttf",BitmapFont.class);
        Sound correctSound = manager.get("sounds/game_correct.wav");
        Sound incorrectSound = manager.get("sounds/game_wrong.wav");
        int answerValue = random.nextInt(98) + 1;
        //Creamos las Balls que seran usadas
        answer = new AnswerBall(Constants.WORLD_WIDTH/10,answerValue, Color.RED,circleTexture,chunkBig,Color.WHITE,correctSound,incorrectSound);
        NumberQuestionBall questionPar = new QParBall(0,0,Constants.WORLD_WIDTH/8f,"PAR",Color.DARK_GRAY,circleTexture,bebasBig,Color.WHITE);
        NumberQuestionBall questionImpar = new QImparBall(0,0,Constants.WORLD_WIDTH/8f,"IMPAR",Color.DARK_GRAY,circleTexture,bebasBig,Color.WHITE);
        QUESTIONS = new Array<ICircle>();
        QUESTIONS.add(questionPar);
        QUESTIONS.add(questionImpar);

        //Se posicionan las QUESTIONS usando un Table
        Table question_table = new Table();
        question_table.setFillParent(true);
        question_table.top().left().add(questionPar).padLeft(questionPar.getRadius()/2).padTop(questionPar.getRadius()/10);
        question_table.add(questionImpar).expandX().right().padRight(questionImpar.getRadius()/2).padTop(questionImpar.getRadius()/10);

        //Creamos nuevo Table para posicionar el TableroPuntuacion
        Table interfaz = new Table();
        interfaz.setFillParent(true);

        BitmapFont tagFont = manager.get("bebasMedium.ttf");
        BitmapFont score_font = manager.get("bebasMedium.ttf");
        tableroPuntuacion = new TableroPuntuacion(tagFont,score_font);
        interfaz.bottom().left().add(tableroPuntuacion).pad(40);

        stage.addActor(question_table);
        stage.addActor(answer);
        stage.addActor(interfaz);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render (float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (!answer.hasActions()) {
            NumberQuestionBall question = (NumberQuestionBall) answer.collided(QUESTIONS);
            if (question != null) {
                if (answer.answer(question))
                    tableroPuntuacion.addPoint();
                else
                    tableroPuntuacion.removePoint();
            }

        }

        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height,true);
    }
}
