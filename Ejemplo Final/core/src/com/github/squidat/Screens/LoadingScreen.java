package com.github.squidat.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.github.squidat.Constants;
import com.github.squidat.ParImpar;

/**
 * Created by juan on 09/09/17.
 */

public class LoadingScreen extends ScreenAdapter {
    private ParImpar game;
    private AssetManager manager;
    private Texture logo;
    private SpriteBatch batch;
    private Viewport viewport;

    public LoadingScreen(ParImpar game) {
        logo = new Texture(Gdx.files.internal("squid.png"));
        logo.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        batch = new SpriteBatch();
        this.game = game;
        viewport = new FitViewport(Constants.WORLD_WIDTH,Constants.WORLD_HEIGHT, new OrthographicCamera());
        manager = game.getAssetManager();
        TextureLoader.TextureParameter smparam = new TextureLoader.TextureParameter();
        smparam.magFilter = Texture.TextureFilter.Linear;
        smparam.minFilter = Texture.TextureFilter.Linear;
        manager.load("hole.png",Texture.class,smparam);
        manager.load("bg1.png",Texture.class,smparam);
        FileHandleResolver resolver = new InternalFileHandleResolver();
        manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        manager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));
        manager.load("sounds/game_correct.wav",Sound.class);
        manager.load("sounds/game_wrong.wav",Sound.class);

        FreetypeFontLoader.FreeTypeFontLoaderParameter bebasParam1 = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        bebasParam1.fontParameters.magFilter =  Texture.TextureFilter.Linear;
        bebasParam1.fontParameters.minFilter =  Texture.TextureFilter.Linear;
        bebasParam1.fontFileName = "fonts/bebas.ttf";
        bebasParam1.fontParameters.size =(int) Constants.WORLD_WIDTH/7;
        bebasParam1.fontParameters.color = Color.WHITE;
        bebasParam1.fontParameters.borderColor = Color.DARK_GRAY;
        bebasParam1.fontParameters.borderWidth = 4;
        manager.load("bebasBig.ttf",BitmapFont.class,bebasParam1);


        FreetypeFontLoader.FreeTypeFontLoaderParameter chunkParam1 = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        chunkParam1.fontParameters.magFilter =  Texture.TextureFilter.Linear;
        chunkParam1.fontParameters.minFilter =  Texture.TextureFilter.Linear;
        chunkParam1.fontFileName = "fonts/Chunkfive.ttf";
        chunkParam1.fontParameters.size =(int) Constants.WORLD_WIDTH/7;
        chunkParam1.fontParameters.color = Color.WHITE;
        chunkParam1.fontParameters.borderColor = Color.BLACK;
        chunkParam1.fontParameters.borderWidth = 4;
        manager.load("chunkBig.ttf",BitmapFont.class,chunkParam1);

        FreetypeFontLoader.FreeTypeFontLoaderParameter bebasParam2 = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        bebasParam2.fontParameters.magFilter =  Texture.TextureFilter.Linear;
        bebasParam2.fontParameters.minFilter =  Texture.TextureFilter.Linear;
        bebasParam2.fontFileName = "fonts/bebas.ttf";
        bebasParam2.fontParameters.size =(int) Constants.WORLD_WIDTH/15;
        manager.load("bebasMedium.ttf",BitmapFont.class,bebasParam2);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();
        batch.draw(logo,Constants.WORLD_WIDTH/2 - logo.getWidth()/2,Constants.WORLD_HEIGHT/2 - logo.getHeight()/2);
        batch.end();
        if (manager.update()) {
            game.setScreen(new GameScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width,height,true);
    }

    public AssetManager getAssetManager() {
        return manager;
    }
}
