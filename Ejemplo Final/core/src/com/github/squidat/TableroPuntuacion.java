package com.github.squidat;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Created by juan on 05/09/17.
 */

public class TableroPuntuacion extends Table {
    private Label tag, score_label ;
    private BitmapFont tag_font,score_font;
    private int puntuacion;

    public TableroPuntuacion(BitmapFont tag_font, BitmapFont score_font) {
        puntuacion = 0;
        this.tag_font = tag_font;
        this.score_font = score_font;
        Label.LabelStyle scoreStyle = new Label.LabelStyle(score_font, scoreColor());
        Label.LabelStyle tagStyle = new Label.LabelStyle(tag_font,Color.WHITE);

        tag = new Label(Constants.SCORE_TAG,tagStyle);
        score_label = new Label(String.valueOf(puntuacion),scoreStyle);
        add(tag);
        add(score_label);
    }

    public void addPoint() {
        puntuacion++;
        update();
    }

    public void removePoint() {
        puntuacion--;
        update();
    }

    public Color scoreColor() {
        if (puntuacion > 0) {
            return Color.GREEN;
        } else if (puntuacion < 0) {
            return Color.RED;
        } else
            return Color.WHITE;
    }

    public void update() {
        score_label.getStyle().fontColor = scoreColor();
        score_label.setText(String.valueOf(puntuacion));
    }
}
