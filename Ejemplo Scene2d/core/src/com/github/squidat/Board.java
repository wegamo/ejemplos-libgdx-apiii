package com.github.squidat;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by juan on 26/08/17.
 *
 * Nota: No existen relaciones de vecindad porque para este ejemplo no se necesitan. Solo necesitamos mostrar las acciones.
 */

public class Board extends Actor {
    private final String TAG = getClass().getSimpleName();
    private ArrayList<Casilla> casillas;
    private int dimension;
    private Random random;
    private float lado;
    private float lado_casilla;
    TextureRegion board_border;

    public Board(int dimension) {
        //this.setDebug(true);
        board_border = NumerMerge.textureAtlas.findRegion("table_cell");
        lado = Constants.WORLD_WIDTH - 2*Constants.WORLD_PADDING;
        this.setBounds(Constants.WORLD_PADDING,(Constants.WORLD_HEIGHT - lado)/2,lado,lado);
        this.dimension = dimension;
        this.lado_casilla = lado/dimension;
        random = new Random();
        crearCasillas();
        this.setTouchable(Touchable.disabled);
    }

    private void crearCasillas() {
        /*
         * Crea y llena la lista de Casillas que pertenecen al board.
         * Por ahora lo hace asumiendo que siempre seran cuadrados, lo que probablemente
         * deberia ser una subclase.
         */

        casillas = new ArrayList<Casilla>();
        float xMove = this.getX();
        float yMove = this.getY();

        for (int i=0; i < dimension; i++) {
            for (int j=0; j <dimension; j++) {
                int casilla_value = random.nextInt(3);
                Casilla casilla = new Casilla(xMove, yMove, casilla_value,lado_casilla,false);
                casillas.add(casilla);
                xMove += lado_casilla;
            }
            yMove += lado_casilla;
            xMove = this.getX();
        }
    }

    public void addToStage(Stage stage) {
        stage.addActor(this);
        for (Casilla c: casillas) {
            stage.addActor(c);
        }
    }


    public float getLado_casilla() {
        return lado_casilla;
    }
}
