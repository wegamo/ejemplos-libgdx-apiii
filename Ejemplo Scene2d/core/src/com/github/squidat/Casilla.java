package com.github.squidat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.awt.Font;
import java.util.Random;

/**
 * Created by juan on 26/08/17.
 * TODO: Cambiar textura atlas a solo una textura y el numero lo diibujamos con un font
 */

public class Casilla extends Actor {
    private final String TAG = getClass().getSimpleName();
    private TextureRegion graphic;
    private int value;
    private boolean throwable;
    private Random random;

    public Casilla(float x, float y, int value, float lado, boolean throwable) {
        random = new Random();
        this.throwable = throwable;
        this.value = value;
        graphic = NumerMerge.textureAtlas.findRegion(String.valueOf(value));
        this.setBounds(x,y,lado,lado);

        if (value == 0) {
            this.addListener(new ClickListener(){
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    GameStage stage = (GameStage) Casilla.this.getStage();
                    Casilla beingThrown = stage.getToThrow();
                    beingThrown.disparar(Casilla.this);
                    //Perdemos el focus intencionalmente para no permitir cambio de direcciones
                    Gdx.input.setInputProcessor(new InputAdapter());
                    return true;
                }
            });
        }
    }

    public Vector2 getPosition() {
        return new Vector2(getX(),getY());
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(graphic,this.getX(),this.getY(),this.getWidth(),this.getHeight());
    }

    public void changeValue(int newValue) {
        this.value = newValue;
        this.graphic = NumerMerge.textureAtlas.findRegion(String.valueOf(value));
    }

    public void disparar(final Casilla targetCas) {
        if (!this.throwable)
            return;

        final Vector2 startingPos = new Vector2(getX(),getY());
        Vector2 targetPos = targetCas.getPosition();
        ThrowAction throwAction = new ThrowAction(targetPos);
        throwAction.setSpeed(Constants.THROW_SPEED);

        //Lanzamos la casilla y creamos una nueva accion que cambie el valor de la casilla en el board
        SequenceAction sequenceAction = new SequenceAction();
        sequenceAction.addAction(throwAction);
        sequenceAction.addAction(new Action() {
            @Override
            public boolean act(float delta) {
                targetCas.changeValue(Casilla.this.value);
                return true;
            }
        });
        sequenceAction.addAction(new Action() {
            @Override
            public boolean act(float delta) {
                //Generar nuevo valor
                int index = random.nextInt(2) + 1;
                Casilla.this.changeValue(index);
                //Mover pieza a su posicion original
                Casilla.this.setPosition(startingPos.x,startingPos.y);
                //Devolvemos el focus al Stage para poder colocar otra Casilla
                Gdx.input.setInputProcessor(targetCas.getStage());
                return true;
            }
        });

        this.addAction(sequenceAction);
    }


}
