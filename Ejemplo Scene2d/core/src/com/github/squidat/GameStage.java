package com.github.squidat;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by juan on 27/08/17.
 */

public class GameStage extends Stage {
    Board board;
    Casilla toThrow;

    public GameStage(Viewport viewport, int board_dimension) {
        super(viewport);
        board = new Board(board_dimension);
        board.addToStage(this);

        float lado_casilla = board.getLado_casilla();
        toThrow = new Casilla(board.getX(),board.getY() - 2*lado_casilla,2,lado_casilla, true);
        this.addActor(toThrow);
    }

    public Casilla getToThrow() {
        return toThrow;
    }
}
